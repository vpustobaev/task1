package by.epam.java.task1.fabric.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import by.epam.java.task1.entity.Point;
import by.epam.java.task1.entity.Tetragon;
import by.epam.java.task1.fabric.FigureFabric;

public class FigureFabricImpl implements FigureFabric {

    private static final Logger logger = LogManager.getLogger(FigureFabricImpl.class);
    private static final String WRONG_COORDINATE_FORMAT = "Some coordinates have wrong format";

    @Override
    public Tetragon createFigureFromSource(String[] coordinates) {

	int x1 = 0;
	int x2 = 0;
	int x3 = 0;
	int x4 = 0;
	int y1 = 0;
	int y2 = 0;
	int y3 = 0;
	int y4 = 0;

	try {
	    x1 = Integer.valueOf(coordinates[0].split(",")[0]);
	    x2 = Integer.valueOf(coordinates[1].split(",")[0]);
	    x3 = Integer.valueOf(coordinates[2].split(",")[0]);
	    x4 = Integer.valueOf(coordinates[3].split(",")[0]);

	    y1 = Integer.valueOf(coordinates[0].split(",")[1]);
	    y2 = Integer.valueOf(coordinates[1].split(",")[1]);
	    y3 = Integer.valueOf(coordinates[2].split(",")[1]);
	    y4 = Integer.valueOf(coordinates[3].split(",")[1]);

	} catch (NumberFormatException e) {
	    logger.error(WRONG_COORDINATE_FORMAT);
	    e.printStackTrace();
	}

	Tetragon tetragon = new Tetragon();

	Point point1 = new Point(x1, y1);
	tetragon.setPoint1(point1);

	Point point2 = new Point(x2, y2);
	tetragon.setPoint2(point2);

	Point point3 = new Point(x3, y3);
	tetragon.setPoint3(point3);

	Point point4 = new Point(x4, y4);
	tetragon.setPoint4(point4);

	return tetragon;

    }

}
