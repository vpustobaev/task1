package by.epam.java.task1.fabric;

import by.epam.java.task1.entity.Tetragon;

public interface FigureFabric {

    public Tetragon createFigureFromSource(String[] coordinates);

}
