package by.epam.java.task1.dao;

import by.epam.java.task1.exception.DaoException;

public interface TetragonDao {

    String[] readInfo(String path) throws DaoException;

}
