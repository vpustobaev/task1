package by.epam.java.task1.dao.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.java.task1.dao.TetragonDao;
import by.epam.java.task1.exception.DaoException;

public class TetragonDaoImpl implements TetragonDao {

    private static final Logger logger = LogManager.getLogger(TetragonDaoImpl.class);
    private static final String FILE_NOT_FOUND = "File wasn't found, check the file path";
    private static final String PROBLEMS_WITH_FILE = "Is the file still accessible?";

    private String[] result = new String[0];

    @Override
    public String[] readInfo(String path) throws DaoException {

	BufferedReader br = null;
	try {
	    br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path))));
	} catch (FileNotFoundException e) {
	    logger.error(FILE_NOT_FOUND);
	    throw new DaoException(FILE_NOT_FOUND, e);
	}

	String line;

	try {
	    while ((line = br.readLine()) != null) {

		String[] tempArray = new String[result.length + 1];

		for (int i = 0; i < tempArray.length - 1; i++) {
		    tempArray[i] = result[i];
		}

		tempArray[result.length] = line;
		result = tempArray;

	    }
	} catch (IOException e) {
	    logger.error(PROBLEMS_WITH_FILE);
	    throw new DaoException(PROBLEMS_WITH_FILE, e);
	}
	try {
	    br.close();
	} catch (IOException e) {

	    e.printStackTrace();
	}
	return result;

    }
}