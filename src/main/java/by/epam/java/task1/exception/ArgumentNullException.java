package by.epam.java.task1.exception;

public class ArgumentNullException extends Exception {

    public ArgumentNullException(String message) {
	super(message);
    }

}
