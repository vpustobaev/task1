package by.epam.java.task1.exception;

public class DaoException extends Exception {

    public DaoException(String message, Throwable cause) {
	super(message, cause);
    }

}
