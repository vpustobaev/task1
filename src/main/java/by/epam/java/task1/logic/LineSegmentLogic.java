package by.epam.java.task1.logic;

import by.epam.java.task1.entity.LineSegment;
import by.epam.java.task1.entity.Point;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class LineSegmentLogic {

    public double calculateLength(LineSegment lineSegment) {

	Point point1 = lineSegment.getPoint1();
	Point point2 = lineSegment.getPoint2();

	double result = sqrt(pow(point2.getX() - point1.getX(), 2) + pow(point2.getY() - point1.getY(), 2));

	return result;
    }

}
