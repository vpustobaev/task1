package by.epam.java.task1.logic;

import by.epam.java.task1.entity.LineSegment;
import by.epam.java.task1.entity.Point;
import by.epam.java.task1.entity.Tetragon;
import by.epam.java.task1.exception.ArgumentNullException;

import static java.lang.Math.abs;
import static java.lang.Math.round;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TetragonLogic {

    private static final Logger logger = LogManager.getLogger(TetragonLogic.class);
    private static final String MISSING_ARGUMENTS = "Some arguments are missing";

    private LineSegmentLogic lineSegmentLogic = new LineSegmentLogic();

    public double calculatePerimeter(Tetragon tetragon) {

	Point point1 = tetragon.getPoint1();
	Point point2 = tetragon.getPoint2();
	Point point3 = tetragon.getPoint3();
	Point point4 = tetragon.getPoint4();

	LineSegment lineSegment1 = null;
	LineSegment lineSegment2 = null;
	LineSegment lineSegment3 = null;
	LineSegment lineSegment4 = null;

	try {
	    lineSegment1 = new LineSegment(point1, point2);
	    lineSegment2 = new LineSegment(point2, point3);
	    lineSegment3 = new LineSegment(point3, point4);
	    lineSegment4 = new LineSegment(point4, point1);
	} catch (ArgumentNullException e) {
	    logger.error(MISSING_ARGUMENTS);
	    e.getMessage();
	}

	double perimetre = lineSegmentLogic.calculateLength(lineSegment1)
		+ lineSegmentLogic.calculateLength(lineSegment2) + lineSegmentLogic.calculateLength(lineSegment3)
		+ lineSegmentLogic.calculateLength(lineSegment4);

	return round(perimetre);

    }

    public double calculateAreaUsingPoints(Tetragon tetragon) {

	Point point1 = tetragon.getPoint1();
	Point point2 = tetragon.getPoint2();
	Point point3 = tetragon.getPoint3();
	Point point4 = tetragon.getPoint4();

	double area = 0.5 * abs((point1.getX() - point2.getX()) * (point1.getY() + point2.getY())
		+ (point2.getX() - point3.getX()) * (point2.getY() + point3.getY())
		+ (point3.getX() - point4.getX()) * (point3.getY() + point4.getY())
		+ (point4.getX() - point1.getX()) * (point4.getY() + point1.getY()));

	return round(area);

    }

    public double calculateAreaUsingTriangleArea(Tetragon tetragon) {

	Point point1 = tetragon.getPoint1();
	Point point2 = tetragon.getPoint2();
	Point point3 = tetragon.getPoint3();
	Point point4 = tetragon.getPoint4();

	TriangleLogic triangleLogic = new TriangleLogic();

	LineSegment side1 = null;
	LineSegment side2 = null;
	LineSegment side3 = null;
	LineSegment side4 = null;
	LineSegment diagonal = null;

	try {
	    side1 = new LineSegment(point1, point2);
	    side2 = new LineSegment(point2, point3);
	    side3 = new LineSegment(point3, point4);
	    side4 = new LineSegment(point4, point1);
	    diagonal = new LineSegment(point1, point3);

	} catch (ArgumentNullException e) {
	    logger.error(MISSING_ARGUMENTS);
	    e.getMessage();
	}

	double angle1 = triangleLogic.calculateAngle(side1, side2, diagonal);
	double angle2 = triangleLogic.calculateAngle(side3, side4, diagonal);

	double triangle1Area = triangleLogic.calculateAreaUsingAngle(side1, side2, angle1);
	double triangle2Area = triangleLogic.calculateAreaUsingAngle(side3, side4, angle2);

	double area = triangle1Area + triangle2Area;

	return round(area);

    }

    public double calculateAreaUsingTriangleArea2(Tetragon tetragon) {

	Point point1 = tetragon.getPoint1();
	Point point2 = tetragon.getPoint2();
	Point point3 = tetragon.getPoint3();
	Point point4 = tetragon.getPoint4();

	TriangleLogic triangleLogic = new TriangleLogic();

	LineSegment side1 = null;
	LineSegment side2 = null;
	LineSegment side3 = null;
	LineSegment side4 = null;
	LineSegment diagonal = null;

	try {
	    side1 = new LineSegment(point1, point2);
	    side2 = new LineSegment(point2, point3);
	    side3 = new LineSegment(point3, point4);
	    side4 = new LineSegment(point4, point1);
	    diagonal = new LineSegment(point1, point3);

	} catch (ArgumentNullException e) {
	    logger.error(MISSING_ARGUMENTS);
	    e.getMessage();
	}

	double triangle1Area = triangleLogic.calculateAreaUsingHalfPerimeter(side1, side2, diagonal);
	double triangle2Area = triangleLogic.calculateAreaUsingHalfPerimeter(side3, side4, diagonal);

	double area = triangle1Area + triangle2Area;

	return round(area);

    }

    public boolean isTetragonQuadrat(Tetragon tetragon) {

	Point point1 = tetragon.getPoint1();
	Point point2 = tetragon.getPoint2();
	Point point3 = tetragon.getPoint3();
	Point point4 = tetragon.getPoint4();

	LineSegment lineSegment1 = null;
	LineSegment lineSegment2 = null;
	LineSegment lineSegment3 = null;
	LineSegment lineSegment4 = null;

	try {
	    lineSegment1 = new LineSegment(point1, point2);
	    lineSegment2 = new LineSegment(point2, point3);
	    lineSegment3 = new LineSegment(point3, point4);
	    lineSegment4 = new LineSegment(point4, point1);
	} catch (ArgumentNullException e) {
	    logger.error(MISSING_ARGUMENTS);
	    e.getMessage();
	}

	double line1 = lineSegmentLogic.calculateLength(lineSegment1);
	double line2 = lineSegmentLogic.calculateLength(lineSegment2);
	double line3 = lineSegmentLogic.calculateLength(lineSegment3);
	double line4 = lineSegmentLogic.calculateLength(lineSegment4);

	return (line1 == line2 && line1 == line2 && line1 == line3 && line1 == line4);

    }

}
