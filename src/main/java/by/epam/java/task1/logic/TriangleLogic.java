package by.epam.java.task1.logic;

import static java.lang.Math.acos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toDegrees;

import by.epam.java.task1.entity.LineSegment;

public class TriangleLogic {

    public double calculateAngle(LineSegment side1, LineSegment side2, LineSegment side3) {

	LineSegmentLogic lineSegmentLogic = new LineSegmentLogic();

	double length1 = lineSegmentLogic.calculateLength(side1);
	double length2 = lineSegmentLogic.calculateLength(side2);
	double length3 = lineSegmentLogic.calculateLength(side3);

	double angle = toDegrees(acos((pow(length1, 2) + pow(length2, 2) - pow(length3, 2)) / (2 * length1 * length2)));

	return angle;

    }

    public double calculateAreaUsingAngle(LineSegment side1, LineSegment side2, double angleInDegrees) {

	LineSegmentLogic lineSegmentLogic = new LineSegmentLogic();

	double length1 = lineSegmentLogic.calculateLength(side1);
	double length2 = lineSegmentLogic.calculateLength(side2);

	double area = 0.5 * length1 * length2 * sin(Math.toRadians(angleInDegrees));

	return area;

    }

    public double calculateAreaUsingHalfPerimeter(LineSegment side1, LineSegment side2, LineSegment side3) {

	LineSegmentLogic lineSegmentLogic = new LineSegmentLogic();

	double length1 = lineSegmentLogic.calculateLength(side1);
	double length2 = lineSegmentLogic.calculateLength(side2);
	double length3 = lineSegmentLogic.calculateLength(side3);

	double halfPerimetre = 0.5 * (length1 + length2 + length3);
	double area = sqrt(
		halfPerimetre * (halfPerimetre - length1) * (halfPerimetre - length2) * (halfPerimetre - length3));

	return area;

    }

}
