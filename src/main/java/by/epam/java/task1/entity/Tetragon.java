package by.epam.java.task1.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import by.epam.java.task1.exception.ArgumentNullException;

public class Tetragon {

    private static final Logger logger = LogManager.getLogger(Tetragon.class);
    private static final String NO_POINT1 = "Point1 is missing";
    private static final String NO_POINT2 = "Point2 is missing";
    private static final String NO_POINT3 = "Point3 is missing";
    private static final String NO_POINT4 = "Point4 is missing";

    private Point point1;
    private Point point2;
    private Point point3;
    private Point point4;

    public Tetragon() {

    }

    public Tetragon(Point point1, Point point2, Point point3, Point point4) throws ArgumentNullException {

	if (point1 == null) {
	    logger.error(NO_POINT1);
	    throw new ArgumentNullException(NO_POINT1);
	}

	if (point2 == null) {
	    logger.error(NO_POINT2);
	    throw new ArgumentNullException(NO_POINT2);
	}

	if (point3 == null) {
	    logger.error(NO_POINT3);
	    throw new ArgumentNullException(NO_POINT3);
	}

	if (point4 == null) {
	    logger.error(NO_POINT4);
	    throw new ArgumentNullException(NO_POINT4);
	}

	this.point1 = point1;
	this.point2 = point2;
	this.point3 = point3;
	this.point4 = point4;
    }

    public Point getPoint1() {
	return point1;
    }

    public Point getPoint2() {
	return point2;
    }

    public Point getPoint3() {
	return point3;
    }

    public Point getPoint4() {
	return point4;
    }

    public void setPoint1(Point point1) {
	this.point1 = point1;
    }

    public void setPoint2(Point point2) {
	this.point2 = point2;
    }

    public void setPoint3(Point point3) {
	this.point3 = point3;
    }

    public void setPoint4(Point point4) {
	this.point4 = point4;
    }

}
