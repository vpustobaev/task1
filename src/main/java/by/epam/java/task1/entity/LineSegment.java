package by.epam.java.task1.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import by.epam.java.task1.exception.ArgumentNullException;

public class LineSegment {

    private static final Logger logger = LogManager.getLogger(LineSegment.class);
    private static final String NO_POINT1 = "Point1 is missing";
    private static final String NO_POINT2 = "Point2 is missing";

    private Point point1;
    private Point point2;

    public LineSegment(Point point1, Point point2) throws ArgumentNullException {

	if (point1 == null) {
	    logger.error(NO_POINT1);
	    throw new ArgumentNullException(NO_POINT1);
	}

	if (point2 == null) {
	    logger.error(NO_POINT2);
	    throw new ArgumentNullException(NO_POINT2);
	}

	this.point1 = point1;
	this.point2 = point2;
    }

    public Point getPoint1() {
	return point1;
    }

    public Point getPoint2() {
	return point2;
    }

}
