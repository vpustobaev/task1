package by.epam.java.task1.util.impl;

import by.epam.java.task1.entity.Point;
import by.epam.java.task1.entity.Tetragon;
import by.epam.java.task1.util.TetragonService;

public class TetragonServiceImpl implements TetragonService {

    @Override
    public Tetragon createTetragonFromSource(String[] coordinates) {

	int x1 = Integer.valueOf(coordinates[0].split(",")[0]);
	int x2 = Integer.valueOf(coordinates[1].split(",")[0]);
	int x3 = Integer.valueOf(coordinates[2].split(",")[0]);
	int x4 = Integer.valueOf(coordinates[3].split(",")[0]);

	int y1 = Integer.valueOf(coordinates[0].split(",")[1]);
	int y2 = Integer.valueOf(coordinates[1].split(",")[1]);
	int y3 = Integer.valueOf(coordinates[2].split(",")[1]);
	int y4 = Integer.valueOf(coordinates[3].split(",")[1]);

	Tetragon tetragon = new Tetragon();

	Point point1 = new Point(x1, y1);
	tetragon.setPoint1(point1);

	Point point2 = new Point(x2, y2);
	tetragon.setPoint2(point2);

	Point point3 = new Point(x3, y3);
	tetragon.setPoint3(point3);

	Point point4 = new Point(x4, y4);
	tetragon.setPoint4(point4);

	return tetragon;

    }

}
