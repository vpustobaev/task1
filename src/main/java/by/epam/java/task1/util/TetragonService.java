package by.epam.java.task1.util;

import by.epam.java.task1.entity.Tetragon;

public interface TetragonService {

    public Tetragon createTetragonFromSource(String[] coordinates);

}
