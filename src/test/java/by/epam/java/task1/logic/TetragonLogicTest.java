package by.epam.java.task1.logic;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import by.epam.java.task1.dao.TetragonDao;
import by.epam.java.task1.dao.impl.TetragonDaoImpl;
import by.epam.java.task1.entity.Tetragon;
import by.epam.java.task1.exception.DaoException;
import by.epam.java.task1.util.TetragonService;
import by.epam.java.task1.util.impl.TetragonServiceImpl;

public class TetragonLogicTest {

	private static final Logger logger = LogManager.getLogger(TetragonLogicTest.class);

	private TetragonLogic tetragonLogic = new TetragonLogic();

	private static Tetragon tetragon;

	@BeforeMethod
	public static void initTetragon() {

		TetragonDao dao = new TetragonDaoImpl();

		String[] coordinates = null;
		logger.info("preparing for tests");

		try {
			coordinates = dao.readInfo("src/test/resources/ConvexTetragon.txt");
		} catch (DaoException e) {
			e.printStackTrace();
		}

		TetragonService service = new TetragonServiceImpl();

		tetragon = service.createTetragonFromSource(coordinates);
		logger.info("Tetragon was created");

	}

	@Test
	public void calculatePerimeterTest() {

		double expected = 12.96;

		double actual = tetragonLogic.calculatePerimeter(tetragon);

		double delta = 0.1;

		assertEquals(expected, actual, delta);
	}

	@Test
	public void calculateAreaUsingPointsTest() {

		double expected = 10;

		double actual = tetragonLogic.calculateAreaUsingPoints(tetragon);

		double delta = 0.01;

		System.out.println("using points = " + actual);
		assertEquals(expected, actual, delta);

	}

	@Test
	public void calculateAreaUsingTriangleAreaTest() {

		double expected = 10;

		double actual = tetragonLogic.calculateAreaUsingTriangleArea(tetragon);

		double delta = 0.01;
		System.out.println("using triangle area = " + actual);
		assertEquals(expected, actual, delta);

	}

	@Test
	public void calculateAreaUsingTriangleArea2Test() {

		double expected = 10;

		double actual = tetragonLogic.calculateAreaUsingTriangleArea2(tetragon);

		double delta = 0.01;
		System.out.println("using triangle area 2 = " + actual);
		assertEquals(expected, actual, delta);

	}

	@Test(enabled = false)
	public void isTetragonQuadratTest() {

		assertTrue(tetragonLogic.isTetragonQuadrat(tetragon));

	}

}
